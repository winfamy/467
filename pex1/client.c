#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <ifaddrs.h>
#include <netdb.h>
#include <sys/time.h>
#include "client.h"

#define PORT 	4240
#define IP		"127.0.0.1"

int main () {
	char c;
	int snum;

	if((snum = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
		perror("socket()");
	}

    struct sockaddr_in server; 
	bzero(&server, sizeof(server));
    int addrlen = sizeof(server); 
    server.sin_family = AF_INET; 
    server.sin_port = htons(PORT); 
    server.sin_addr.s_addr = inet_addr(IP);

	struct timeval t;
	t.tv_sec = 5;
	t.tv_usec = 0;
	if (setsockopt(snum, SOL_SOCKET, SO_RCVTIMEO, &t, sizeof(t)) < 0) {
		perror("Error setting socket timeout.");
		exit(1);
	}

	while (1) {
		print_opts();
		scanf(" %c", &c);
		handle_cmd(snum, (int)(c - '0'), server, addrlen);
	}
}

void print_opts () {
	printf("\n\nWelcome to Grady's PEX1\n");
	printf("Available Options:\n");
	printf("1.\t\tList\n");
	printf("2.\t\tStream\n");
	printf("3.\t\tExit\n");
	printf("Enter option: ");
}

int handle_cmd(int sockfd, int opt, struct sockaddr_in server, int addrlen) {
	char mp3[1024];
	char cmd[1024];
	// if (connect(sockfd, addr, sizeof(addr)) < 0) {
	// 	perror("Error: connect failed");
	// 	return -1;
	// }

	switch (opt) {
		case 1:
			strcpy(cmd, "LIST_REQUEST");
			sendto(sockfd, cmd, strlen(cmd), 0, (struct sockaddr *)&server, sizeof(server));
			int bytes = recvfrom(sockfd, mp3, 1024, 0, &server, &addrlen);

			// FORMAT LIST_REPLY server response
			printf("%s\n", mp3);
			break;
		case 2:
			printf("stream\n");
			printf("Enter mp3 name: ");
			scanf("%s", mp3);
			printf("creating command %s\n", mp3);
			strcpy(cmd, "START_STREAM\n");
			strcat(cmd, mp3);
			printf("created string %s\n", cmd);
			if(sendto(sockfd, cmd, strlen(cmd), 0, (struct sockaddr *)&server, sizeof(server)) == 0) {
				perror("socket connect failure");
				exit(1);
			}

			// HANDLE COMMAND_ERROR (file not found response)
			streamFile(sockfd, mp3, server, addrlen);
			break;
		case 3:
			close(sockfd);
			exit(0);
		default:
			printf("\nNot an available option... try again.\n\n\n");
	}

	return 0;
}

void streamFile(int sockfd, char * filename, struct sockaddr_in server, int addrlen) {
	char buf[1024];
	char newFilename[256];
	strcpy(newFilename, "recv_");
	strcat(newFilename, filename);
	FILE * f = fopen(filename, "w");
	while (1) {
		int bytes = recvfrom(sockfd, buf, 1024, 0, (struct sockaddr *)&server, &addrlen);
		// handle command_error here
		
		if (bytes < 0) {
			perror("socket connect failure");
			return;
		}

		// STRIP out STREAM_DATA\n
		fwrite(buf, sizeof(char), bytes, f);
	}

	fclose(f);
}